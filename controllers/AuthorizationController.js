import {Router} from "express";
import RegistrationUserValid from "../validations/RegistrationUserValid.js";
import {query} from "express-validator";
import TelegramBotClient from "../clients/TelegramBotClient.js";
import SecurityService from "../services/SecurityService.js";
import ConfirmRegFormValid from "../validations/ConfirmRegFormValid.js";
import LoginFormValid from "../validations/LoginFormValid.js";
import ErrorHandle from "../validations/ErrorHandle.js";

const authorizationRouter = Router()

class AuthorizationController {

    static async signUp(req, res) {
        res.send(await SecurityService.saveUser(req.body))
        res.end()
    }

    static async pinkNumber(req, res) {
        res.send(await TelegramBotClient.hasPhoneNumber(req.query.number))
        res.end()
    }

    static async confirmCode(req, res) {
        res.send(await SecurityService.confirmRegistration(req.body))
        res.end()
    }

    static async signIn(req, res) {
        const authUser = await SecurityService.login(req.body)

        if (!authUser) {
            return res.status(400).json({message: 'Data is not correct or user not found'}).end()
        }
        res.json(authUser).end()
    }
}


authorizationRouter.post('/sign-up',
    RegistrationUserValid.handle,
    ErrorHandle.handle,
    AuthorizationController.signUp)

authorizationRouter.get('/number',
    query('number')
        .exists()
        .withMessage('Phone number is not exists')
        .matches(/\d{10,15}/)
        .withMessage('Phone number is not match \\d{10,15}'),
    ErrorHandle.handle,
    AuthorizationController.pinkNumber)

authorizationRouter.post('/confirm-code',
    ConfirmRegFormValid.handle,
    ErrorHandle.handle,
    AuthorizationController.confirmCode)

authorizationRouter.post('/login',
    LoginFormValid.handle,
    ErrorHandle.handle,
    AuthorizationController.signIn)


export default authorizationRouter