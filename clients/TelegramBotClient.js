import fetch from 'node-fetch'
import config from 'config'


const CONFIG_BOT = config.get('telegramBot')

class TelegramBotClient {

    static async hasPhoneNumber(phoneNumber) {
        const URI = `${CONFIG_BOT.telegramBotDbUrl}?${CONFIG_BOT.tokenParamName}=${CONFIG_BOT.token}&${CONFIG_BOT.phoneNumberParamName}=${phoneNumber}`

        const respond = await fetch(URI);

        if (respond.status > 399) {
            return false
        }

        return await respond.text()
    }

    static async sendMessage(telegramMessage) {
        const formData = new URLSearchParams()

        formData.set(CONFIG_BOT.tokenParamName, CONFIG_BOT.token)
        formData.set(CONFIG_BOT.jsonParamName, JSON.stringify(telegramMessage))

        const respond = await fetch(CONFIG_BOT.telegramBotUrl, {
            method: 'POST',
            body: formData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        })

        if (respond.status > 399) {
            return false
        }

        return await respond.text()
    }
}

export default TelegramBotClient