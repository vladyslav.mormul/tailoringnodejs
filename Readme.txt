Fronted : https://gitlab.com/vladyslav.mormul/tailoringreact
Telegram bot : https://gitlab.com/vladyslav.mormul/telegrambot

SHORT STATEMENT OF THE TASK
Customer enters the site, having previously registered, places an order for the tailoring of specific products with the criteria he needs. The client can also leave comments under the order. The administrator checks the order, if it can be done, then he sets the price of the order and the deadline
