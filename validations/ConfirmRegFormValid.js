import {body} from "express-validator";

class ConfirmRegFormValid {
    static get handle() {
        return valid
    }
}

const valid = [
    body('phoneNumber')
        .isString()
        .withMessage('Must be string')
        .exists()
        .withMessage('Must be exists')
        .matches(/\d{10,15}/)
        .withMessage('Must has only numbers and length between 10 and 15'),
    body('code')
        .exists()
        .withMessage('Must be exists')
        .matches('[A-Za-z0-9]{20,20}')
        .withMessage('Code length must be 20 and contains only A-Z,a-z,0-9')
]

export default ConfirmRegFormValid