import {LocalDateTime} from '../tools/tools.js'
import UserCode from '../models/UserCode.js'
import MongoFilter from '../filters/MongoFilter.js'

class UserCodeRepository {
    static #MINUTES_FOR_WORK = 15

    static async insertCode(user, code) {
        const confirmCode = new UserCode({
            value: code,
            dateOfCreation: LocalDateTime(new Date()),
            userPhoneNumber: user.phoneNumber
        })

        const answer = await confirmCode.save()

        return !answer.errors
    }

    static async updateCode(userPhoneNumber, codeValue) {
        const date = new Date()
        date.setMinutes(date.getMinutes() - UserCodeRepository.#MINUTES_FOR_WORK)

        const query = MongoFilter.and(
            [
                {value: MongoFilter.eq(codeValue)},
                {userPhoneNumber: MongoFilter.eq(userPhoneNumber)},
                {active: MongoFilter.eq(true)},
                {dateOfCreation: MongoFilter.gt(LocalDateTime(date))}
            ]
        )

        return (await UserCode.updateOne(query, {"$set": {active: false}})).modifiedCount !== 0
    }
}

export default UserCodeRepository