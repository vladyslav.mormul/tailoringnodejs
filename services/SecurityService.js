import UserRepository from '../repository/UserRepository.js'
import UserCodeService from "./UserCodeService.js";
import TelegramMessage from "../tools/TelegramMessage.js";
import TelegramBotClient from "../clients/TelegramBotClient.js";
import bcrypt from 'bcryptjs'
import config from "config";
import {getDate, LocalDateTime} from "../tools/tools.js";
import jwt from "jsonwebtoken";

const strength = config.get('strength')
const jwtWord = config.get('jwtWord')

class SecurityService {
    static get #USER_NOT_FOUND() {
        return false
    }

    static async saveUser(userForm) {
        if (await UserRepository.isBooked(userForm.email, userForm.phoneNumber)) {
            return false;
        }

        const salt = await bcrypt.genSalt(Number.parseInt(strength));

        userForm.password = await bcrypt.hash(userForm.password, salt)

        const userId = await UserRepository.save(userForm)

        if (!userId) {
            return false;
        }

        const code = UserCodeService.generateCode()
        const text = `${userForm.firstname} ${userForm.lastname}, your confirmation code is ${code}`

        if (!await UserCodeService.insertCode(userForm, code)) {
            return false;
        }

        const telegramMessage = SecurityService.#getMessage(code, text, userForm.phoneNumber)

        return await TelegramBotClient.sendMessage(telegramMessage)
    }

    static async confirmRegistration(confirmRegForm) {
        if (await UserCodeService.updateCode(
            confirmRegForm.phoneNumber,
            confirmRegForm.code
        ) === SecurityService.#USER_NOT_FOUND) {
            return SecurityService.#USER_NOT_FOUND;
        }

        const user = await SecurityService.#getUserByUsername(confirmRegForm.phoneNumber)

        user.userState = "REGISTERED"

        return await UserRepository.update(user)
    }

    static async login(signInUser) {
        const user = (await UserRepository.findBy({
            username: signInUser.phoneNumber,
            active: true,
            userStates: ['REGISTERED']
        }))[0]

        if (!user || await SecurityService.#comparePassword(signInUser.password, user.password)) {
            return null
        }

        delete user.password
        delete user.userState
        delete user.active
        delete user.dataOfRegistration

        return {
            token: jwt.sign(
                {user},
                jwtWord,
                {
                    expiresIn: '1h'
                }
            ),
            user
        }
    }

    static #getMessage(code, text, phoneNumber) {
        return TelegramMessage.of(phoneNumber, text, [
            TelegramMessage.entityBuilder()
                .type("spoiler")
                .length(code.length)
                .offset(text.length - code.length)
                .build()
        ])
    }

    static async #getUserByUsername(phoneNumber) {
        const users = await UserRepository.findBy({
            username: phoneNumber,
            userStates: ['REGISTRATION'],
            dataOfRegistration: {
                from: LocalDateTime(getDate(15))
            },
            orderBy: 'dataOfRegistration',
            direction: 'DESC'
        })

        if (users.length > 0) {
            return users[0]
        }
        throw `User with phone number '${phoneNumber}' not found`
    }

    static async #comparePassword(inputPass, actualPass) {
        const salt = await bcrypt.genSalt(Number.parseInt(strength));

        const hashInputPass = await bcrypt.hash(inputPass, salt)

        return bcrypt.compare(hashInputPass, actualPass)
    }
}

export default SecurityService