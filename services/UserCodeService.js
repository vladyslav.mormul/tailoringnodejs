import UserCodeRepository from '../repository/UserCodeRepository.js'

class UserCodeService {
    static get #CODE_SIZE() {
        return 20
    }

    static get #CHARACTERS() {
        return 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    }

    static get #CHARACTERS_LENGTH() {
        return UserCodeService.#CHARACTERS.length
    }

    static async insertCode(user, code) {
        return UserCodeRepository.insertCode(user, code)
    }

    static async updateCode(userPhoneNumber, codeValue) {
        return UserCodeRepository.updateCode(userPhoneNumber, codeValue)
    }

    static generateCode() {
        let result = '';

        for (let i = 0; i < UserCodeService.#CODE_SIZE; i++) {
            result += UserCodeService.#CHARACTERS
                .charAt(Math.floor(
                    Math.random() * UserCodeService.#CHARACTERS_LENGTH));
        }
        return result;
    }
}

export default UserCodeService